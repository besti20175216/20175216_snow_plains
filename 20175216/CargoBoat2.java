public class CargoBoat2{
     int realContent;  //装载的重量
     int minContent;   //最大装载量
     public void setminContent(int c) {
         minContent = c;
     }
     public void loading(int m) throws DangerException2{
       realContent += m;
       if(realContent>minContent) {
          throw new DangerException2();
       }
       System.out.println("目前装载了"+realContent+"吨货物");
     }
}
