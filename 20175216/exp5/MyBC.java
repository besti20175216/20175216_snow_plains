import java.util.Stack;

public class MyBC {
    static Stack<Character> op = new Stack<>();
    public static String getrp(String s){
        char[] arr = s.toCharArray();
        int len = arr.length;
        String out = "";
        for(int i =0;i<len;i++){
            char ch = arr[i];
            if(ch == ' ') continue;
            if(ch>='0'&&ch<='9'){
                out +=ch;
                continue;
            }
            if(ch =='(') op.push(ch);
            if(ch == '+'|| ch=='-'){
                while(!op.empty()&&(op.peek()!='('))
                    out +=op.pop();
                op.push(ch);
                continue;
            }
            if(ch=='*'||ch=='/'){
                while(!op.empty()&&(op.peek()=='*'||op.peek()=='/'))
                    out+=op.pop();
                op.push(ch);
                continue;
            }
            if(ch == ')'){
                while(!op.empty()&&op.peek()!='(')
                    out += op.pop();
                op.pop();
                continue;
            }
        }
        while(!op.empty()) out += op.pop();
        return out;
    }
}

