
import java.util.Scanner;
import static org.junit.Assert.*;
/**
 * Created by lxkj on 2017/6/2.
 */

import java.util.Scanner;

public class MyBCTest  {
    public static void main (String[] args) {

        String expression1=null,expression2=null, again=null;

        int result,i=0,length=0;

        try
        {
            Scanner in = new Scanner(System.in);

            do
            {
                MyDC2 evaluator = new MyDC2();
                MyBC turner = new MyBC();
                System.out.println ("Enter a valid midfix expression: ");
                expression1 = in.nextLine();
                //System.out.println(expression1);
                expression2 = turner.turn(expression1);
                while(expression2.charAt(i)!='\0'){
                    length++;
                    i++;
                }
                //length 表示字符串包括‘\0’的字符个数
                expression2 = expression2.substring(1,length-1);
                //System.out.println(expression2);
                result = evaluator.evaluate (expression2);
                System.out.println();
                System.out.println ("That expression equals " + result);

                System.out.print ("Evaluate another expression [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}