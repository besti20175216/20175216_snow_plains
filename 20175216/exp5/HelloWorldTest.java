import junit.framework.TestCase;
import org.junit.Test;

public class HelloWorldTest extends TestCase {
    @Test
    public void testNormal(){
        assertEquals("==20175216",HelloWorld.main(20175216));
    }
    @Test
    public void testerror(){
        assertEquals("!=20175216",HelloWorld.main(20175202));
    }
}
