import java.util.Scanner;

public class Kaisa {
    public static void main(String[] args) {
        System.out.print("请输入明文：");
        Scanner a=new Scanner(System.in);
        String b=a.nextLine();
        System.out.print("请输入秘钥：");
        Scanner c=new Scanner(System.in);
        int key=c.nextInt();
        Encrypt(b, key);
    }

    public static void Encrypt(String str,int k){
        String s="";
        for (int i = 0; i < str.length(); i++) {
            char c=str.charAt(i);
            if(c>='a'&&c<='z'){
                c+=k%26;
                if(c<'a')
                    c+=26;
                if(c>'z')
                    c-=26;

            }else if(c>='A'&&c<='Z'){
                c+=k%26;
                if(c<'A')
                    c+=26;
                if(c>'Z')
                    c-=26;
            }
            s+=c;
        }
        System.out.println(str+" 加密为： "+s);
    }
}
