#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

void main(int argc, char *argv[])
{
    struct stat state;

    stat(argv[1], &state);

    printf("  文件：‘%s'\n", argv[1]);
    printf("  大小：%lld\t", (long long)state.st_size);
    printf("块：%lld\t", (long long)state.st_blocks);
    printf("IO块：%ld\t", (long)state.st_blksize);
    switch(state.st_mode & S_IFMT)
    {
    case S_IFBLK:
        printf("块设备文件");
        break;
    case S_IFCHR:
        printf("字符设备文件");
        break;
    case S_IFDIR:
        printf("目录");
        break;
    case S_IFIFO:
        printf("管道文件");
        break;
    case S_IFLNK:
        printf("符号链接文件");
        break;
    case S_IFREG:
        printf("普通文件");
        break;
    case S_IFSOCK:
        printf("套接字文件");
        break;
    default:
        break;
    }
    printf("\n");

    printf("设备：%xh/%ldd\t", (long)state.st_dev, (long)state.st_dev);
    printf("Inode：%ld\t", (long)state.st_ino);
    printf("硬链接：%ld\n", (long)state.st_nlink);
    printf("权限：(%o)\t", (unsigned int)(state.st_mode & ~S_IFMT));
    printf("Uid：(%ld)\t", (long)state.st_uid);
    printf("Gid：(%ld)\n", (long)state.st_gid);
    printf("最近访问：%s", ctime(&state.st_atim));
    printf("最近更改：%s", ctime(&state.st_ctim));
    printf("最近改动：%s", ctime(&state.st_mtim));
    printf("创建时间：-");
    printf("\n");
}