
public class Complex {
    private double RealPart;//复数的实部
    private double ImagePart;//复数的虚部
    public Complex() {}
    public Complex(double R,double I){
        setRealPart(R);
        setImagePart(I);
    }
    public void setRealPart(double R){
        RealPart=R;//设置复数的实部
    }
    public void setImagePart(double I){
        ImagePart=I;//设置复数的虚部
    }
    public double getRealPart(){
        return RealPart;//返回复数的实部
    }
    public double getImagePart(){
        return ImagePart;//返回复数的虚部
    }
    Complex ComplexAdd(Complex a) {
        Complex New =new Complex();
        New.RealPart=this.RealPart+a.RealPart;
        New.ImagePart=this.ImagePart+a.ImagePart;
        return New;
    }
    Complex ComplexSub(Complex a){
        Complex New =new Complex();
        New.RealPart=this.RealPart-a.RealPart;
        New.ImagePart=this.ImagePart-a.ImagePart;
        return New;
    }
    Complex ComplexMulti(Complex a){
        Complex New =new Complex();
        New.RealPart=this.RealPart*a.RealPart-this.ImagePart*a.ImagePart;
        New.ImagePart=this.ImagePart*a.RealPart+this.RealPart*a.ImagePart;
        return New;
    }
    Complex ComplexDiv(Complex a){
        double sum=a.ImagePart*a.ImagePart+a.RealPart*a.RealPart;
        Complex b = new Complex(a.getRealPart()/sum, -a.getImagePart()/sum);
        return ComplexMulti(b);
    }
    @Override
    public String toString() {
        if(ImagePart>0.0)
            return RealPart+"+"+ImagePart+"i";
        else if(ImagePart==0.0)
            return RealPart+" ";
        else
            return RealPart+""+ImagePart+"i";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Complex complex = (Complex) o;
        return Double.compare(complex.getRealPart(), getRealPart()) == 0 && Double.compare(complex.getImagePart(), getImagePart()) == 0;
    }
}
