import java.util.Scanner;
public class Array {
    public static void main(String [] args){
        int[] arr = new int[4];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("请输入数字：");
            arr[i] = input();
        }
        System.out.println("\r\n输入：");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        System.out.println("\r\n输出：");
        for (int i = arr.length-1; i >= 0; i--) {
            System.out.print(arr[i]*arr[i] + "\t");
        }

    }
    public static int input(){
        int x = Integer.MIN_VALUE ;
        try{
            x = new Scanner(System.in).nextInt();
        } catch(Exception e){
            System.out.print("\r\n输入错误，请重新输入。");
            x = input();
        }
        return x;
    }
}
