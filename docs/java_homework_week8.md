# 学号 20175216 《Java程序设计》第七周学习总结

## 教材学习内容总结
第八章要点：
语法与继承架构
使用 try、catch
Java中所有信息都会被打包为对象，如果愿意，可以尝试（try）捕捉（catch）代表错误的对象后做一些处理
```
try{
    ...(需要尝试捕捉的程序代码)
}
catch(... ex){
    ...(发生错误时执行的代码)
}
```
JVM 会尝试执行 try 区块中的程序代码。如果发生错误，执行流程会跳离错误发生点，然后比较 catch 括号中声明的类型，是否符合被抛出的错误对象类型，如果是的话，就执行catch 区块中的程序代码

try、catch 用法举例：
```
import java.util.*;

public class Average2
{
    public static void main(String[] args)
    {
       try
       {
           Scanner console = new Scanner(System.in);
           double sum = 0;
           int count = 0;
           while (true)
           {
               int number = console.nextInt();
               if (number ==0)
               {
                   break;
               }
               sum += number;
               count++;
           }
           System.out.printf("平均 %.2f%n",sum / count);
       }
       catch (InputMismatchException ex)
       {
           System.out.println("必须输入整数");
       }
    }
}
```
有时错误可以在捕捉处理之后，尝试恢复程序正常执行流程，例如：
```
import java.util.*;

public class Average3
{
    public static void main(String[] args)
    {
        Scanner console = new Scanner(System.in);
        double sum = 0;
        int count = 0;
        while (true)
        {
            try
            {
                int number = console.nextInt();
                if (number == 0)
                {
                    break;
                }
                sum += number;
                count++;
            }
            catch (InputMismatchException ex)
            {
                System.out.printf("略过非整数输入：%s%n", console.next());
            }
        }
        System.out.printf("平均 %.2f%n", sum / count);
    }
}
```
异常继承架构
Throwable 定义了取得错误信息、堆栈追踪等方法，有两个子类：java.lang.Error 与 java.lang.Exception

异常处理：程序设计本身的错误，建议使用 Exception 或其子类实例来表现，所以通常称错误处理为异常处理

单就语法与继承架构上来说，如果某个方法声明会抛出 Throwable 或子类实例，只要不是属于 Error、ava.lang.RuntimeException 或其子类实例，你就必须明确使用 try、catch语法加以处理，或者用 throws 声明这个方法会抛出异常，否则会编译失败

受检异常：Exception 或其子对象，但非属于 RuntimeException 或其子对象，称为受检异常

执行期异常（非受检异常）：因为编译程序不会强迫一定得在语法上加以处理，亦称为非受检异常

规则表达式：String 的 matches() 方法中设定了 "\d*"，这是规则表示式，表示检查字符串中的字符是不是数字，若是则 matches() 返回 true

如果父类异常对象在子类异常对象前被捕捉，则 catch 子类异常对象的区块将永远不不会被执行

多重捕捉语法：
```
try{
        做一些事...
    }catch(IOException | InterruptedException | ClassCastException e){
//catch 区块会在发生 IOException、InterruptedException、ClassCastException 时执行
        e.printStackTrace();
    }
catch 括号中列出的异常不得有继承关系，否则会发生编译错误
catch or throw?
```
如果方法设计流程中发生异常，而设计时没有充足的信息知道该如何处理，那么可以抛出异常，让调用方法的客户端来处理。为了告诉编译程序这个事实，必须用 throws 声明此方法会抛出的异常类型或父类型，编译程序才会让你通过编译。例如：
```
public class FileUtil {
    public static String readFile(String name)
        throws FileNotFoundException{
        StringBuilder text = new StringBuilder();
        Scanner console = new Scanner(new FileInputStream(name));
        while(console.hasNext()){
            text.append(console.nextLine())
                    .apend('\n');
        }
        return text.toString();
    }
}
```


自定义异常类别时，可以继承Throw、Error 或 Exception或其子类，如果不是继承自Error或 RuntimeException，那么就会是受检异常

自定义受检异常：

public class CustomizedException extends Exception{
    ...
}
错误发生时：
*    无足够信息处理异常：就现有信息处理完异常后，重新抛出异常

*    已针对错误做了某些处理：考虑自定义异常，用以更精确地表示出未处理的错误

*    客户端有能力处理未处理的错误：自定义受检异常、填入适当错误信息并重新抛出，并在方法上使用 `throws`加以声明

*    客户端没有准备好就调了方法造成未处理错误：自定义受检异常、填入适当错误信息并重新抛出
常用实用类：
String类：不可变类，一些看起来能够改变String的方法其实都是创建了一个带有方法所赋予特性的新String。StringBuffer类为字符串缓冲类，可变。
String类覆盖了Object类的equals()方法，而StringBuffer没有。String类的toString()方法是返回当前String实例本身的引用，而StringBuffer类的toString()方法返回的则是当前缓冲区中所有字符内容的新的String对象的引用。使用StringBuffer可以减少JVM创建String对象的次数，减少动态分配和回收内存的次数，提高程序的性能。
Scanner类：获取用户的输入
Math类：数学运算类
Random类：生成随机数
Data类：以毫秒数表示特定的日期。sysout(new Date()) 返回系统日期
DataFormat类：用于定制日期的格式。
Calendar类：设置和读取日期的其中一部分。
BigDecimal类：精准计算

代码调试中的问题和解决过程
问题1
```
public Test() throws RepletException {
    try {
      System.out.println("Test this Project!")
    }
    catch (Exception e) {
      throw new Exception(e.toString());
    }
  }
```
throw和throws的区别有什么啊？这里改成throw可以吗？
解决方案：
从网上查询得知，throws是用来声明一个方法可能抛出的所有异常信息，而throw则是指抛出的一个具体的异常类型。通常在一个方法（类）的声明处通过throws声明方法（类）可能抛出的异常信息，而在方法（类）内部通过throw声明一个具体的异常信息。throws通常不用显示的捕获异常，可由系统自动将所有捕获的异常信息抛给上级方法。throw则需要用户自己捕获相关的异常，而后在对其进行相关包装，最后在将包装后的异常信息抛出。

问题2： 什么是多重捕捉，要注意什么问题？
解决2： 一开始看书时，觉得多重捕捉就是讲多个异常合在一起捕捉，遵循"DRY"(Don't Repeat Yourself)原则，使程序更简洁。但是多重捕捉时出现问题，发现是没有注意异常继承架构。在多重捕捉的时候，catch括号中列出的异常不得有继承关系，否则会发生编译错误。

## [代码托管]


- 代码量截图：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0412/162911_985c5874_4784444.png "第七周.PNG")
## 上周考试错题总结
上周考试无错题。

    
## 其他（感悟、思考等，可选）
虚拟机一定记得备份，否则虚拟机崩了之后就会丢失，非常麻烦。
在学习教材的时候，发现其实自己编一遍代码与对着书编代码还是有很大区别的，自己编一遍的话，会发现很多只对着书编所察觉不到的细节，而这些细节也正是我们全面认识这个代码的关键点所在
## 学习进度条
|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:    |:-----:|
| 目标        | 5000行            |   30篇           |  400小时             |       |
| 第一周      | 32/32             |   1/1            |  12/12              |       |
| 第二周      | 302/334           |   1/2            |  10/22              |       |
| 第三周      | 771/1105           |   1/3           |  15/37              |       |
| 第四周      | 540/1645           |   1/4           |  14/51              |       |
| 第五周      | 399/2044           |   1/5           |  13/64              |       |
| 第六周      | 1112/3156         |   1/6           |  13/77              |       |
| 第七周      | 653/3809         |    1/7           |  13/90              |       |
- 计划学习时间:15小时

- 实际学习时间:13小时

- 改进情况：
这周学习了第八章知识，通过运行代码明白各代码的具体含义，体会到了娄老师上课一直提到的“做中学，学中做”。

## 参考资料

-  [Java学习笔记(第8版)](http://book.douban.com/subject/26371167/) 

-  [《Java学习笔记(第8版)》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)
