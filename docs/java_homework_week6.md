2018-2019-2 20175216  实验一《Java开发环境的熟悉》实验报告
实验一Java开发环境的熟悉-1
实验要求：
1 建立“自己学号exp1”的目录 
2 在“自己学号exp1”目录下建立src,bin等目录
3 javac,java的执行在“自己学号exp1”目录
4 提交 Linux或Window或macOS中命令行下运行Java的全屏截图， 运行结果包含自己学号等证明自己身份的信息
5 代码提交码云
运行结果截图：![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/174942_58f180db_4784444.png "exp1.PNG")
实验一Java开发环境的熟悉-2
利用IDEA进行调试
实验要求：
参考http://www.cnblogs.com/rocedu/p/6371315.html，在Window环境中 IDEA中调试设置条件断点
1、调试程序首先要会设置断点和单步运行。设置断点就是要在设置断点的行号旁用鼠标单击一下（或使用Ctrl+F8快捷键）
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/182111_1043ccae_4784444.png "实验一2（1）.png")
2、开始调试程序，单击Run--Debug
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/182122_a6eb7adc_4784444.png "实验一2（2）.png")
3、单步运行让程序运行到第8行，单步运行有两种：Step Into(快捷捷F7)和Step Over（快捷捷F8
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/182417_f0a0cbe3_4784444.png "实验一2（3）.png")
4、设置条件断点
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/182408_bab9b9a8_4784444.png "实验一2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/182427_c53c511b_4784444.png "32fdd5f145a7654aa0f5a0a6bb96dade.png")
实验一Java开发环境的熟悉-3
练习提交自己练习项目的码云链接和运行结果的截图。
16mod9=7 7+1=8
8. 产生一个随机数，让用户猜测，猜错了告诉用户是大了还是小了。 并进行测试（正常情况，异常情况，边界情况）
实验过程：
1、设计随机数的区间0-5216
2、对输入的数进行判断（猜大了、猜小了、猜对了、不在输入范围内、输入格式不对）

import java.util.Random;
import java.util.Scanner;
public class suijishu {
       public static void main (String[] args){
           Random shu = new Random();
           int a = shu.nextInt(5216);
           System.out.println("请猜一下5216内的随机数：");
           Scanner input = new Scanner(System.in);
           try{
               for(int i=0;;i++) {
                   int b = input.nextInt();
                   if (b == a) {
                       System.out.println("恭喜你，你猜对了！");
                       break;
                   } else if (b < a&&b >= 0) {
                       System.out.println("对不起，你猜错了，你猜得有点小");
                   } else if(b > a&&b <= 5216) {
                       System.out.println("对不起，你猜错了，你猜的有点大");
                   }
                   else {
                       System.out.println("输入的数不在既定范围，请重新输入");
                   }
               }
           }catch (Exception e) {
               System.out.println("输入有错误，请重新输入");
           }
       }
}
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/210739_62a5a464_4784444.png "实验一3（一）_看图王.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/210749_31b26b1d_4784444.png "实验一3（二）_看图王.png")
实验总结
遇到的问题：1、IDEA无法和码云连接![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/210802_b0214b3b_4784444.png "问题一png.png")
2、输入格式不对![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/210810_99b82477_4784444.png "问题二.png")
解决方法：我本来想用else解决的，但没能解决。在学到第七章的try-catch就把这个问题解决了![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/211027_582063c1_4784444.jpeg "try1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/211046_23f09751_4784444.png "try-catch1.png")

对IDEA有了初步的了解，同时对自己学习的知识进行了一定的检查，发现自己还有许多小的细节不够细心，希望在之后的学习中可以敲出更有质量的代码，自己也越来越喜欢写程序的过程。
步骤	耗时	百分比
需求分析	10min	6.7%
设计	25min	16.7%
代码实现	90min	60%
测试	10min	6.7%
分析总结	15min	10%
