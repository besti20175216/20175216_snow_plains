# 20175216张雪原  《Java程序设计》第1周学习总结

## 教材学习内容总结
1.Java平台简介

2.系统环境的设置
1)设置系统变量JAVA_HOME；
2）系统环境Path的配置；
3)系统环境classpath的配置。

3.jdk的安装和使用
1）开发工具位于子目录bin中；
2）Java运行环境位于子目录jre中；
3)附加库位于子目录lib中；
4）C头文件位于子目录include中。

书上很多东西需要自己去实际运用才可以掌握，实践与理论相结合才是最合适的学习方法，这和老师说的“做中学，学中做”是一个道理
## 教材学习中的问题和解决过程
1、jdk 环境变量
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/234218_2c2ab6fd_4784444.png "Y{3BD@L5$LY1E0~RADXT[[P.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0303/234122_d2897c76_4784444.png "M}T`QM0WYJJ2N7R%{`[YS)E.png")




## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           |  400小时            |       |
| 第一周      | 32/32             |   1/1            |  12/12                |       |



- 计划学习时间:15小时

- 实际学习时间:12小时




## 参考资料

-  [Java学习笔记(第8版)](http://book.douban.com/subject/26371167/) 

-  [《Java学习笔记(第8版)》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)
-  ...