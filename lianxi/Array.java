public class Array{
	public static void main(String args[]){
		int arr[] = new int[4];
		System.out.println("请输入:");
		for (int i = 0; i< arr.length; i++){
		    arr[i] = input();
		}
		System.out.println("输出结果:\r\n");
		for (int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println("\r\n");
		for (int i = arr.length-1; i>= 0; i--){
			System.out.print(arr[i]*arr[i] + "\t");
		}
	}
}
    public static int input(){
        int x = Integer.MIN_VALUE ;
        try{
            x = new Scanner(System.in).nextInt();
        } catch(Exception e){
            System.out.print("\r\nerror");
            x = input();
        }
        return x;
    }
}

