import java.sql.*;

public class MySQL1 {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world", "root", "");
        if (con == null) {
            return;
        }
        String sqlStr = "select*from city where population>8017521";
        try {
            sql = con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String countryCode = rs.getString(3);
                String district = rs.getString(4);
                int population = rs.getInt(5);
                System.out.printf("%d\t", id);
                System.out.printf("%s\t", name);
                System.out.printf("%s\t", countryCode);
                System.out.printf("%s\t", district);
                System.out.printf("%d\n", population);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Error:" + e);
        }


    }
}
