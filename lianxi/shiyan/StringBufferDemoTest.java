import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {

    StringBuffer a = new StringBuffer("StringBuffer");
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBuffer");

    @Test
    public void testCharAt() throws Exception {
        assertEquals('S', a.charAt(0));
        assertEquals('g', a.charAt(5));
        assertEquals('r', a.charAt(11));
    }

    @Test
    public void testcapacity() throws Exception {
        assertEquals(28, a.capacity());
        assertEquals(40, b.capacity());
        assertEquals(52, c.capacity());
    }

    @Test
    public void testlength() throws Exception {
        assertEquals(12, a.length());//验证字符串a的长度
        assertEquals(24, b.length());
        assertEquals(36, c.length());
    }

    @Test
    public void testindexOf() {
        assertEquals(6, a.indexOf("Buff"));
        assertEquals(5, b.indexOf("gBu"));
        assertEquals(10, c.indexOf("erS"));
    }
}
